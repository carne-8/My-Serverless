module MyServerless.API.HttpHandlers

open System
open System.Text
open FSharp.Data
open FsToolkit.ErrorHandling
open Giraffe
open MyServerless.Common

type NotionFilter =
    { Filter:
        {|
            Property: string
            Text: {| Equals: string option
                     Is_empty: bool option |}
        |}
    }

type NotionResponse =
    { Results:
        {| Properties:
            Map<string,
                    {| Id: string
                       Url: string option
                       Title: {| Plain_text: string option |} list option
                    |}> |} list }


let notionSecret = Config.getString "notion:secret"
let databaseId = Config.getString "notion:shortener:databaseId"

let notionRoutePropertyId = Config.getString "notion:shortener:routePropertyId"
let notionRedirectRoutePropertyId = Config.getString "notion:shortener:redirectRoutePropertyId"

let notionUrl = sprintf "https://api.notion.com/v1/databases/%s/query" databaseId
let headers =
    [ "Authorization", sprintf "Bearer %s" notionSecret
      "Content-Type", "application/json"
      "Notion-Version", "2021-08-16" ]

let redirectFromNotionPath : HttpHandler =
    handleContextTR (fun ctx ->
        taskResult {
            let endOfPath =
                SubRouting.getNextPartOfPath ctx
                |> String.trimc '/'

            let filter =
                match endOfPath |> String.IsNullOrEmpty with
                | true ->
                    { Filter =
                        {| Property = notionRoutePropertyId
                           Text = {| Is_empty = Some true; Equals = None |} |} }
                | false ->
                    { Filter =
                        {| Property = notionRoutePropertyId
                           Text = {| Is_empty = None; Equals = Some endOfPath |} |} }
                |> Json.toString

            try
                let notionResponse =
                    Http.Request (
                        notionUrl,
                        httpMethod = "POST",
                        headers = headers,
                        body = HttpRequestBody.TextRequest filter
                    )

                let! responseParsed =
                    match notionResponse.Body with
                    | HttpResponseBody.Text jsonResponse ->
                            jsonResponse
                            |> Json.fromString<NotionResponse>
                            |> Result.mapError Response.internalError
                    | HttpResponseBody.Binary _ -> "Incorrect response from notion API." |> Response.internalError |> Result.Error

                match responseParsed.Results |> List.isEmpty with
                | true -> return Response.notFound
                | false ->

                    let! redirectBaseUrl =
                        responseParsed.Results.Head.Properties
                        |> Map.pick (fun propertyId value ->
                            if propertyId = notionRedirectRoutePropertyId
                            then Some value.Url
                            else None
                        )
                        |> Result.requireSome (Response.internalError "Redirect URL does not exist.")

                    let redirectUrl =
                        let uriBuilder = UriBuilder redirectBaseUrl
                        let queryStringBuilder =
                            ctx.Request.Query
                            |> Seq.fold (fun (sb: StringBuilder) value ->
                                sb.Append (if sb.Length = 0 then "?" else "&") |> ignore

                                (sb, value.Value)
                                ||> Seq.fold (fun sb queryStringValue ->
                                    sb.Append(value.Key |> Uri.EscapeDataString)
                                      .Append("=")
                                      .Append(queryStringValue |> Uri.EscapeDataString)
                                )
                            ) (StringBuilder(uriBuilder.Query))

                        uriBuilder.Query <- queryStringBuilder |> string
                        uriBuilder |> string

                    return Response.redirectTo true redirectUrl
            with
            | error -> return (error |> Response.internalError)
        }
    )

let shortener : HttpHandler =
    GET >=> subRouteCi "/short" redirectFromNotionPath