﻿namespace System.Threading.Tasks


[<AutoOpenAttribute>]
module TaskExtensions =
    open System

    module Task =
        let run<'a> (fn: unit -> 'a) = Task.Run<'a>(Func<'a>(fn))

        let runTask<'a> (fn: unit -> Task<'a>) = Task.Run<'a>(Func<_>(fn))

        let runT (t: #Task) = Task.Run(fun () -> t)

        let asTask (t: Task<_>) = t :> Task

        let asUnitTask (t: Task) =
            t.ContinueWith(Func<Task, unit>(ignore), TaskContinuationOptions.OnlyOnRanToCompletion)

    type TaskResultFunc<'a, 'b, 'c> = 'a -> Task<Result<'b, 'c>>