﻿namespace FsToolkit.ErrorHandling

open System.Threading.Tasks

[<AutoOpen>]
module ResultExtensions =

    [<RequireQualifiedAccess>]
    module Result =
        let catch fn prm =
            try
                fn prm |> Result.Ok
            with
            | e -> Result.Error e


        let requireSomeResult err r =
            r |> Result.bind (Result.requireSome err)

        let either fSuccess fError =
            function
            | Result.Ok x -> fSuccess x
            | Result.Error x -> fError x

        let returnOrFail x =
            x |> either id (failwithf "Result.Error %A")


    [<RequireQualifiedAccess>]
    module Option =
        let bindTaskResultOption (fn: _ -> Task<Result<_ option, _>>) (x: 'a option) =
            match x with
            | None -> None |> Result.Ok |> Task.FromResult
            | Some v -> v |> fn

    [<RequireQualifiedAccess>]
    module TaskResultOption =
        let bindTaskResultOption (fn: _ -> Task<Result<_ option, _>>) (x: Task<Result<'a option, _>>) =
            taskResult {
                match! x with
                | Some v -> return! v |> fn
                | None -> return None
            }


    [<RequireQualifiedAccess>]
    module TaskResult =
        open FSharp.Control.Tasks

        let either fSuccess fError (r: Task<Result<_, _>>) =
            taskResult {
                match! r with
                | Result.Ok x -> return fSuccess x
                | Result.Error x -> return fError x
            }

        let eitherResult fSuccess fError (r: Task<Result<_, _>>) =
            task {
                match! r with
                | Result.Ok x -> return fSuccess x
                | Result.Error x -> return fError x
            }

        let Ok x = Result.Ok x |> Task.FromResult
        let Error x = Result.Error x |> Task.FromResult


        let catch fn prm =
            taskResult {
                try
                    return fn prm
                with
                | e -> return! Result.Error e
            }

        let requireSomeResult err r =
            r
            |> TaskResult.bind (Result.requireSome err >> Task.singleton)

        let runAndForget (t: Task<'a>) =
            Task.runTask <| fun () ->
                task {
                    try
                        let! _ = t
                        ()
                    with
                    | _ -> ()
                }

        let returnOrFail (t: Task<_>) = t |> Task.map Result.returnOrFail

    [<RequireQualifiedAccess>]
    module TaskOption =
        let ofResult tr = tr |> Task.map Option.ofResult
        let ofResultError tr = tr |> Task.map Option.ofResultError