module Json

open System.IO
open System.Threading.Tasks
open Newtonsoft.Json
open Newtonsoft.Json.Linq
open Thoth.Json.Net
open FsToolkit.ErrorHandling

let inline encoder<'T> = Encode.Auto.generateEncoderCached<'T>(CaseStrategy.CamelCase)
let inline decoder<'T> = Decode.Auto.generateDecoderCached<'T>(CaseStrategy.CamelCase)

let fromStream<'T> (stream:Stream) =
    use streamReader = new StreamReader(stream)
    use jsonReader = new JsonTextReader(streamReader)
    let json = JToken.LoadAsync(jsonReader)
    json
    |> Task.map (Decode.fromValue "$" (decoder<'T>))

let toString<'T> (data: 'T) =
    data
    |> encoder
    |> Encode.toString 0

let fromString<'T> (data: string) =
    data
    |> Decode.fromString decoder<'T>