module TestServer

open System
open Giraffe

open Microsoft.AspNetCore
open Microsoft.AspNetCore.Hosting
open Microsoft.AspNetCore.TestHost
open Microsoft.AspNetCore.Builder

let createTestServer app =

    let builder =
        WebHost
            .CreateDefaultBuilder()
            .Configure(Action<IApplicationBuilder>(fun appBuilder ->
                appBuilder
                    .UseWebSockets()
                    .UseGiraffe(app)
            ))
            .ConfigureServices(fun services ->
                services
                    .AddGiraffe()
                |> ignore
            )

    new TestServer(builder)


let createDefaultServer () = Run.app |> createTestServer