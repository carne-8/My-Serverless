module Program

open Expecto
open FSharp.Control.Tasks

[<EntryPoint>]
let main args =

    System.Environment.SetEnvironmentVariable("AZURE_FUNCTIONS_ENVIRONMENT", "Development")
    System.Environment.SetEnvironmentVariable("AzureWebJobsScriptRoot", System.Reflection.Assembly.GetExecutingAssembly().Location |> System.IO.Path.GetDirectoryName)

    let t =
        task {
            return runTestsInAssemblyWithCLIArgs [] args
        }

    t.Wait()

    t.Result
